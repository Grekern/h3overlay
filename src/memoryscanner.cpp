#include "memoryscanner.h"

#include <tlhelp32.h>
#include <psapi.h>
#include <wchar.h>
#include <stdint.h>
#include <memory>

#include <QObject>
#include <QDebug>
#include <QRegularExpression>
#include <QRegularExpressionMatch>


#define POINTER_CASTING(addr) reinterpret_cast<LPCVOID>(static_cast<intptr_t>(addr))

MemoryScanner::MemoryScanner(Settings *settings, QObject *parent):
    QObject(parent),
    winLossCounted(false),
    multiplayerType(UNKNOWN_MULTIPLAYER),
    frameCounterNotInMatch(0),
    settings(settings)
{
    this->pollRateTimer.setInterval(100);
    connect(&(this->pollRateTimer), &QTimer::timeout, this, &MemoryScanner::updateState);

    this->processCheckTimer.setInterval(2000);
    connect(&(this->processCheckTimer), &QTimer::timeout, this, &MemoryScanner::checkProcess);

    clearBuffers();
    memset(&lastOpponent, 0, MAX_PLAYER_NAME_LENGTH);
}

bool MemoryScanner::init()
{
    clearBuffers();
    this->pollRateTimer.start();
    this->processCheckTimer.start();
    return this->proc.init();
}

void MemoryScanner::clearBuffers()
{
    memset(&this->localPlayer, 0, sizeof(PlayerStruct));
    memset(&this->opponentPlayer, 0, sizeof(PlayerStruct));
    memset(&this->matchInfo, 0, sizeof(MatchInfoStruct));
    this->proc.reset();

    emit playerUpdated(displayInfo);
}

void MemoryScanner::checkProcess()
{
    this->proc.attachToProcess();
}

bool MemoryScanner::isNameSameWhenLowered(const std::array <char, MAX_PLAYER_NAME_LENGTH > &name1,
                                          const std::array <char, MAX_PLAYER_NAME_LENGTH > &name2)
{
    for (size_t i = 0; i < name1.size();i++)
    {
        // They match if we find the string null terminator
        if(name1[i] == 0 && name2[i] == 0)
        {
            return true;
        }
        if(tolower(name1[i]) != tolower(name2[i]))
        {
            return false;
        }
    }
    return true;
}


void MemoryScanner::extractColoredName(PlayerStruct &player)
{
    strncpy(&player.nameColor[0], "#E0CE75", 8);
    std::array <char, MAX_PLAYER_NAME_LENGTH > tmp;
    memcpy(&tmp[0], &player.info.playerName[0], MAX_PLAYER_NAME_LENGTH);
    for(int i = 0; i < MAX_PLAYER_NAME_LENGTH - 1; i++)
    {
        if(player.info.playerName[i] >= 0x17 && player.info.playerName[i] < 0x20)
        {
            switch(player.info.playerName[i])
            {
            case GameTextColor::Green:
                strncpy(&player.nameColor[0], "#83E16F", 8);
                break;
            case GameTextColor::Red:
                strncpy(&player.nameColor[0], "#F15D5A", 8);
                break;
            case GameTextColor::Pink:
                strncpy(&player.nameColor[0], "#F68D8A", 8);
                break;
            case GameTextColor::Gray:
                strncpy(&player.nameColor[0], "#8D8C8A", 8);
                break;
            /* Black is quite hard to see...
             * case GameTextColor::Black:
                strncpy(&player.nameColor[0], "#000201", 8);
                break;*/
            case GameTextColor::Blue:
                strncpy(&player.nameColor[0], "#5987B7", 8);
                break;
            case GameTextColor::Purple:
                strncpy(&player.nameColor[0], "#C055FF", 8);
                break;
            }

            // Extract the name from the color encoding.
            memcpy(&player.info.playerName[0],
                   &tmp[i+1],
                   MAX_PLAYER_NAME_LENGTH - i);
        }
    }
    for(int i = 0; i < MAX_PLAYER_NAME_LENGTH; i++)
    {
        // The color coding ends with a "}" (ACSII 0x7D) after the text.
        // Replace it with 0 so it doesn't show up in the display.
        if(player.info.playerName[i] == '}')
        {
            player.info.playerName[i] = 0;
        }
    }
    if(settings->getDisplayNameColor() == false)
    {
        strncpy(&player.nameColor[0], "#E0CE75", player.nameColor.size());
    }
}

bool MemoryScanner::updatePlayersInfo()
{
    if (!this->proc.processInfo.handle)
    {
        return false;
    }

    PlayerBaseStruct players[8];
    bool localAlreadySet = false;
    if(!readMemory(POINTER_CASTING(this->proc.processInfo.playerSectionAddress),
                    &players[0],
                    sizeof(players)))
    {
        qWarning() << "Could not read Player id";
        return false;
    }

    for(PlayerBaseStruct &player: players)
    {
        if(player.isHuman && player.isLocal && localAlreadySet == false)
        {
            localAlreadySet = true;
            memcpy(&this->localPlayer.info, &player, sizeof(PlayerBaseStruct));

        }
        else if(player.isHuman)
        {
            // Don't copy data if this is local multiplayer and the setting to
            // read local multiplayer games is set to false
            if(player.isLocal && this->settings->getDisplayLocalMatch() == false)
            {
                if(this->multiplayerType == UNKNOWN_MULTIPLAYER)
                {
                    this->multiplayerType = LOCAL_MULTIPLAYER;
                }
                return false;
            }
            memcpy(&this->opponentPlayer.info, &player, sizeof(PlayerBaseStruct));
        }
    }

    if(this->multiplayerType == UNKNOWN_MULTIPLAYER)
    {
        this->multiplayerType = ONLINE_MULTIPLAYER;
    }

    extractColoredName(this->localPlayer);
    extractColoredName(this->opponentPlayer);
    this->localPlayer.rating = getPlayerProfile(this->localPlayer).rating;
    this->opponentPlayer.rating = getPlayerProfile(this->opponentPlayer).rating;

    return true;
}


bool MemoryScanner::checkIfInMatch()
{
    bool wasInMatch = this->proc.processInfo.isInMatch;

    if (this->proc.processInfo.statusStruktAddress == 0)
    {
        return false;
    }
    if (readMemory(POINTER_CASTING(this->proc.processInfo.statusStruktAddress),
                   &status,
                   sizeof(StatusWindowStruct)) == false)
    {
        return false;
    }

    this->proc.processInfo.isInMatch = static_cast<bool>(status.isInMatchPointer);

    if(this->proc.processInfo.isInMatch == false && wasInMatch == true)
    {
        this->tradeResult[Left] = "0";
        this->tradeResult[Right] = "0";
        this->winLossCounted = false;
        this->frameCounterNotInMatch = status.frameCounter;
    }

    if(this->frameCounterNotInMatch != status.frameCounter)
    {
        this->frameCounterNotInMatch = 0;
    }
    this->proc.processInfo.isFinishedLoading = this->frameCounterNotInMatch != status.frameCounter;

    return this->proc.processInfo.isInMatch;
}

void MemoryScanner::setDisplayInfo(PlayerStruct &player, size_t playerNumber)
{

    if(playerNumber >= this->displayInfo.player.size())
    {
        qCritical() << "Player number value can't exceed " << this->displayInfo.player.size();
        return;
    }

    if(this->displayInfo.player[playerNumber].money.isEmpty())
    {
        this->displayInfo.player[playerNumber].money = "0";
    }

    this->displayInfo.player[playerNumber].name = &player.info.playerName[0];
    if(heroMap.size() > matchInfo.startHero[player.info.color])
    {
        this->displayInfo.player[playerNumber].hero = heroMap[matchInfo.startHero[player.info.color]];
    }
    if(townMap.size() > this->matchInfo.startTown[player.info.color])
    {
        this->displayInfo.player[playerNumber].town = townMap[matchInfo.startTown[player.info.color]];
    }
    this->displayInfo.player[playerNumber].rating = QString::number(player.rating);
    this->displayInfo.player[playerNumber].wins = QString::number(player.wins);
    this->displayInfo.player[playerNumber].nameColor = QString(&player.nameColor[0]);

    this->displayInfo.winner = &player.winningColor[0];
    if(colorMap.size() > player.info.color)
    {
        this->displayInfo.player[playerNumber].playerColor = colorMap[player.info.color];
    }
    this->displayInfo.player[playerNumber].playerNumer = player.info.color;

    if(player.info.isLocal)
    {
        this->displayInfo.ratingDelta = QString::number(player.ratingDelta);
    }

    this->displayInfo.newMatchToRegister = player.newMatchToRegister;
}


#define HOTA_EXE_TO_CHAT_OFFSET 0x29d800
#define MAX_CHAT_MESSAGE_COUNT 20

bool MemoryScanner::scanForTradeMessages()
{
    chatStruct chat;
    if(!readMemory(POINTER_CASTING(this->proc.processInfo.exeBaseAddress + HOTA_EXE_TO_CHAT_OFFSET),
                    &chat,
                    sizeof(chat)))
    {
        qWarning() << "Could not read chat log.";
        return false;
    }

    // We are not in a lobby, reset trade result.
    if(chat.isInMatchLobbyPointer == 0)
    {
        this->displayInfo.player[Left].money = "0";
        this->displayInfo.player[Right].money = "0";
        return true;
    }

    messageStruct messages[MAX_CHAT_MESSAGE_COUNT];

    if(!readMemory(POINTER_CASTING(chat.messagesPointer),
                    messages,
                    sizeof(messages)))
    {
        qWarning() << "Could not read chat messages";
        return false;
    }

    QRegularExpressionMatch match;

    // The text we are searching for is in the lobby chat windows. In english
    // the text looks like the following row:
    //    <SYSTEM> RED: +1234 Blue: -1234
    // To support other languages, we don't search for the text, just the
    // symbols and numbers.
    static QRegularExpression re("^<.*>: .+: ([+-]?[0-9]+) +.+: ([+-]?[0-9]+)");
    QStringList money = QStringList() << "0" << "0" << "0";

    // In case the system message regarding trade has been overwritten in
    // the circular buffer, we default to the last known trade amount.
    if(chat.messageCount > 0)
    {
        money[1] = tradeResult[Left];
        money[2] = tradeResult[Right];
    }

    for(uint32_t i = chat.messageCount; i > 0; --i)
    {
        uint32_t circularBufferIndex = (i - 1 + chat.messageStart) % MAX_CHAT_MESSAGE_COUNT;
        QString messagePayload(&messages[circularBufferIndex].message[0]);
        match = re.match(messagePayload);
        if (match.hasMatch())
        {
            money = match.capturedTexts();
            break;
        }
    }
    if(chat.messageCount > 0)
    {
        tradeResult[Left] = money[1];
        tradeResult[Right]= money[2];
    }

    return true;
}


#define HOTA_EXE_TO_MAP_INFO_POINTER 0x299538
#define MAP_INFO_TO_START_HERO_TOWN_OFFSET 0x1f6b0

bool MemoryScanner::updateMatchInfo()
{
    uint64_t mapInfoAddr = this->proc.followPointer(this->proc.processInfo.exeBaseAddress + HOTA_EXE_TO_MAP_INFO_POINTER);

    if(!readMemory(POINTER_CASTING(mapInfoAddr + MAP_INFO_TO_START_HERO_TOWN_OFFSET),
                    &this->matchInfo,
                    sizeof(this->matchInfo)))
    {
        qWarning() << "Could not read match info.";
        return false;
    }
    return true;
}


#define HOTA_EXE_TO_POPUP_ACTIVE 0xF1728
#define HOTA_EXE_TO_POPUP 0x2844C
// This seems a bit unstable...
#define HOTA_EXE_TO_POPUP_OFFSET 0x614

void MemoryScanner::updateMatchResult()
{
    if(this->winLossCounted || this->proc.processInfo.isInMatch == false)
    {
        return;
    }

    // This is just a pointer which seems to be set as soon as a popup window
    // appers. Use it to figure out when we should start searching for the
    // Win/Loss popup.
    uint32_t anyActivePopup = this->proc.followPointer(this->proc.processInfo.exeBaseAddress + HOTA_EXE_TO_POPUP_ACTIVE);

    anyActivePopup = this->proc.followPointer(anyActivePopup);
    if (anyActivePopup == 0)
    {
        return;
    }

    uint32_t popupStruct = this->proc.followPointer(this->proc.processInfo.exeBaseAddress + HOTA_EXE_TO_POPUP);
    // This memory location does not always contain the pointer to the desired
    // destination, it might not be a pointer at all so we have to check it.
    if (popupStruct == 0 || isAddressReadable(POINTER_CASTING(popupStruct)) == false)
    {
        return;
    }

    // This seems a bit unstable...
    //uint32_t popupWindowText = this->proc.followPointer(popupStruct + HOTA_EXE_TO_POPUP_OFFSET);
    uint32_t popupWindowText = popupStruct + HOTA_EXE_TO_POPUP_OFFSET;

    // This memory location does not always contain the pointer to the desired
    // destination, it might not be a pointer at all so we have to check it.
    if (popupWindowText == 0 || isAddressReadable(POINTER_CASTING(popupWindowText)) == false)
    {
        return;
    }

    char buffer[50];
    if(!readMemory(POINTER_CASTING(popupWindowText),
                   buffer,
                   sizeof(buffer)))
    {
        qWarning() << "Could not read WIN/LOSS.";
        return;
    }

    // The first byte should be the color of the popup text.
    // 0x17 == green color == win
    // 0x18 == red color == loss
    if(buffer[0] != GameTextColor::Green &&
         buffer[0] != GameTextColor::Red)
    {
        return;
    }
    // Example of english popup:
    //   YOU WIN!
    //   Rating: +25
    // Not searching for english character to support non-english game clients.
    static QRegularExpression re("\\!.*: ([+-][0-9]+)$",
                                 QRegularExpression::DotMatchesEverythingOption);
    QRegularExpressionMatch match;
    // Skipping first byte as it is the text color
    QString ratingText(&buffer[1]);
    match = re.match(ratingText);
    if (match.hasMatch() && match.capturedTexts().length() > 1)
    {
        QStringList matches(match.capturedTexts());
        this->localPlayer.ratingDelta = matches[1].toInt();
        this->localPlayer.newMatchToRegister = true;
        this->opponentPlayer.newMatchToRegister = true;
        this->winLossCounted = true;

        memset(&this->localPlayer.winningColor[0], 0, this->localPlayer.winningColor.size());
        memset(&this->opponentPlayer.winningColor[0], 0, this->localPlayer.winningColor.size());

        // Did the local player win (green text)?...
        if(buffer[0] == GameTextColor::Green)
        {
            strncpy(&this->localPlayer.winningColor[0],
                    colorMap[this->localPlayer.info.color],
                    this->localPlayer.winningColor.size() - 1);
            this->localPlayer.wins++;
        }
        else
        {
            strncpy(&this->localPlayer.winningColor[0],
                    colorMap[this->opponentPlayer.info.color],
                    this->localPlayer.winningColor.size() - 1);
            this->opponentPlayer.wins++;
        }
        this->opponentPlayer.winningColor = this->localPlayer.winningColor;
    }
}


QList<MEMORY_BASIC_INFORMATION> MemoryScanner::enumAllVirtualMemory()
{
    size_t start = 0;
    QList<MEMORY_BASIC_INFORMATION> mbis;
    MEMORY_BASIC_INFORMATION mbi;
    while ((start < pow(2,32)) &&
           (VirtualQueryEx(this->proc.processInfo.handle,
                           POINTER_CASTING(start),
                           &mbi,
                           sizeof(mbi)) == sizeof(mbi)))
    {
        if((mbi.State == MEM_COMMIT) &&
            (mbi.Protect & (PAGE_READWRITE | PAGE_READONLY)) &&
            (mbi.Protect & PAGE_GUARD) == 0)
        {
            mbis.append(mbi);
        }

        start = reinterpret_cast<size_t>(mbi.BaseAddress) + reinterpret_cast<size_t>(mbi.RegionSize);
        if(mbi.RegionSize == 0)
        {
            break;
        }
    }
    return mbis;
}


void MemoryScanner::findLocalProfileAddress()
{
    // If opponent is local, we might not be logged in to the online lobby so
    // it might be a bad idea to try and find the lobby data.
    if(this->opponentPlayer.info.isLocal)
    {
        return;
    }
    if(this->proc.processInfo.localPlayerProfileAddress)
    {
        return;
    }

    QList<MEMORY_BASIC_INFORMATION> readableMBI = enumAllVirtualMemory();
    // Seaching ALL available game memory
    foreach(MEMORY_BASIC_INFORMATION mbi, readableMBI)
    {
        std::unique_ptr<uint8_t[]> buffer(new uint8_t[mbi.RegionSize]);
        if(!readMemory(mbi.BaseAddress,
                        &buffer.get()[0],
                        mbi.RegionSize))
        {
            qWarning() << "Could not scan memory for local profile.";
            return;
        }

        // TODO fix so it can scan for profile which is overlapping two regions.
        // Seaching each memory region for "identifiers"
        // I don't trust it to be 4 bytes alligned so checking all bytes (pos++ instead of pos+=4)...
        for (uint32_t pos = 0;pos <= mbi.RegionSize - sizeof(lobbyProfile);pos++)
        {
            lobbyProfile *profile = reinterpret_cast<lobbyProfile*>(&buffer[pos]);
            // Random "identifiers" (I have no idea what they are) that seems
            // to be the same value in the lobby profile info.
            // In identifier1, the 34 seems to be unique for youreself,
            // almost everyone else have 33. Some have 0x00000000 for some reason
            if(profile->identifier1 == 0x0034002E &&
                profile->identifier2 == 0x00000041 &&
                // Also check that the name matches the local player, to verify that we are in the correct place
                isNameSameWhenLowered(this->localPlayer.info.playerName, profile->name))
            {
                this->proc.processInfo.localPlayerProfileAddress = (static_cast<uint32_t>(reinterpret_cast<uint64_t>(mbi.BaseAddress)) + pos);
                return;
            }
        }
    }
}


lobbyProfile MemoryScanner::getLocalPlayerProfile()
{
    lobbyProfile profile;
    memset(&profile, 0, sizeof(profile));

    if(this->proc.processInfo.localPlayerProfileAddress == 0)
    {
        return profile;
    }
    memset(&profile, 0, sizeof(lobbyProfile));
    if(!readMemory(POINTER_CASTING(this->proc.processInfo.localPlayerProfileAddress),
                    &profile,
                    sizeof(lobbyProfile)))
    {
        qWarning() << "Could not read local player.";
        return profile;
    }
    return profile;
}


// This might break when H3 HD updates
#define HOTA_HD_DLL_TO_PLAYER_VECTOR 0x1450
#define PLAYER_VECTOR_TO_VECTOR_START 0x40

lobbyProfile MemoryScanner::getOnlinePlayerProfile(std::array <char, MAX_PLAYER_NAME_LENGTH> playerName)
{
    static uint32_t previousProfileAddress = 0;
    lobbyProfile profile;

    // Don't rescan the entire player list if it is still the same user.
    if(previousProfileAddress != 0 &&
        readMemory(POINTER_CASTING(previousProfileAddress),
                   &profile,
                   sizeof(lobbyProfile)))
    {
        // The game sometimes changed the capitalization of some characters.
        // Check if they are the same when converted to lower case...
        if(isNameSameWhenLowered(profile.name, playerName))
        {
            return profile;
        }
    }

    uint32_t playerVectorStartAddress = this->proc.followPointer(this->proc.processInfo.hdDLLBaseAddress + HOTA_HD_DLL_TO_PLAYER_VECTOR);

    playerVectorStartAddress += PLAYER_VECTOR_TO_VECTOR_START;

    memset(&profile, 0, sizeof(lobbyProfile));

    // There are two addresses here, a start address and an end address of the
    // vector containing pointers to profiles.
    uint32_t playerVectors[2];
    if(!readMemory(POINTER_CASTING(playerVectorStartAddress),
                           playerVectors,
                           sizeof(playerVectors)))
    {
        qWarning() << "Could not read player list.";
        return profile;
    }
    size_t vectorSize = playerVectors[1] - playerVectors[0];

    // Each element in the vector is a 32 bit pointer. Storing it on the heap
    // due to this vector can be quite large (could blow the stack).
    std::unique_ptr<uint32_t[]> buffer(new uint32_t[vectorSize/sizeof(uint32_t)]);

    // This is reading an array with pointers, each pointer points to a
    // buffer for each specific player.
    if(!readMemory(POINTER_CASTING(playerVectors[0]),
                    &buffer.get()[0],
                    vectorSize))
    {
        qWarning() << "Could not read vector list.";
        return profile;
    }

    for(size_t i = 0; i < vectorSize/sizeof(uint32_t);i++)
    {
        // Following each pointer and fetching the player profile.
        if(!readMemory(POINTER_CASTING(buffer.get()[i]),
                        &profile,
                        sizeof(lobbyProfile)))
        {
            qWarning() << "Could not read opponent profile.";
        }
        else
        {
            // The game sometimes changed the capitalization of some characters.
            // Check if they are the same when converted to lower case...
            if(isNameSameWhenLowered(profile.name, playerName))
            {
                previousProfileAddress = buffer.get()[i];
                return profile;
            }
        }
    }
    // We reach here if no matching player was found which would be a bit odd.
    memset(&profile, 0, sizeof(lobbyProfile));
    return profile;
}


void MemoryScanner::resetWinsIfAppropriate()
{
    // Only reset score if it is online multiplayer, local multiplayer is
    // often used after an online match.
    if (opponentPlayer.info.isLocal == false)
    {
        // Only reset score if there is a new opponent
        if (isNameSameWhenLowered(this->opponentPlayer.info.playerName,
                                  this->lastOpponent) == false)
        {
            this->localPlayer.wins = 0;
            this->opponentPlayer.wins = 0;
            this->lastOpponent = this->opponentPlayer.info.playerName;
        }
    }
}


lobbyProfile MemoryScanner::getPlayerProfile(const PlayerStruct &player)
{
    lobbyProfile profile;

    if(player.info.isLocal)
    {
        // The local player does not exist in the same list as all other lobby
        // players. Has to search in another place.
        // Correspond to top left corner in lobby, I think...
        profile = getLocalPlayerProfile();
        if(isNameSameWhenLowered(player.info.playerName, profile.name))
        {
            return profile;
        }
    }
    else
    {
        profile = getOnlinePlayerProfile(player.info.playerName);
        if(isNameSameWhenLowered(player.info.playerName, profile.name))
        {
            return profile;
        }
    }

    memset(&profile, 0, sizeof(lobbyProfile));
    return profile;
}


bool MemoryScanner::isAddressReadable(LPCVOID address)
{
    MEMORY_BASIC_INFORMATION mbi;
    VirtualQueryEx(this->proc.processInfo.handle, address, &mbi, sizeof(mbi));
    return ((mbi.State == MEM_COMMIT) &&
            (mbi.Protect & (PAGE_READWRITE | PAGE_READONLY)) &&
            (mbi.Protect & PAGE_GUARD) == 0);
}


bool MemoryScanner::readMemory(LPCVOID address, LPVOID buffer, size_t size)
{
    size_t bytesRead = 0;
    bool success = ReadProcessMemory(this->proc.processInfo.handle,
                                     address,
                                     buffer,
                                     size,
                                     &bytesRead);
    if(success == false)
    {
        DWORD lastError = GetLastError();
        qWarning() << "Could not scan memory. Error code: " << lastError;
    }
    if(bytesRead != size)
    {
        qWarning() << "Expected bytes: " << size << ", bytes read:" << bytesRead;
    }
    return success;
}

void MemoryScanner::updateState()
{
    if (this->proc.processInfo.handle == 0 || this->proc.isGameStillRunning() == false)
    {
        return;
    }
    if(checkIfInMatch() == false)
    {
        this->proc.processInfo.localPlayerProfileAddress = 0;
        this->multiplayerType = UNKNOWN_MULTIPLAYER;
        // We are not in a match so we might be in the lobby,
        // scan for trade info in chat.
        if(scanForTradeMessages() == false)
        {
            // Failing the scan for trade means that we might no longer be
            // attached to the game (perhaps game was closed). We try
            // to attach again.
            this->proc.attachToProcess();
        }
        return;
    }


    if(updatePlayersInfo())
    {
        if(this->multiplayerType == LOCAL_MULTIPLAYER &&
                this->settings->getDisplayLocalMatch() == false)
        {
            return;
        }
        if(this->proc.processInfo.isFinishedLoading)
        {
            findLocalProfileAddress();
        }

        resetWinsIfAppropriate();
        updateMatchInfo();
        updateMatchResult();

        displayInfo.player[0].money = this->tradeResult[localPlayer.info.color > opponentPlayer.info.color];
        displayInfo.player[1].money = this->tradeResult[opponentPlayer.info.color > localPlayer.info.color];
        setDisplayInfo(localPlayer, 0);
        setDisplayInfo(opponentPlayer, 1);

        // Only trigger a GUI update if new data is available to reduce CPU usage.
        if(memcmp(&localPlayer, &prevLocalPlayer, sizeof(PlayerStruct)) != 0 ||
           memcmp(&opponentPlayer, &prevRemotePlayer, sizeof(PlayerStruct)) != 0)
        {
            emit playerUpdated(displayInfo);
            this->localPlayer.newMatchToRegister = false;
            this->opponentPlayer.newMatchToRegister = false;
        }
        // Copy last "frame" so we can check if something was updated next frame.
        prevLocalPlayer = localPlayer;
        prevRemotePlayer = opponentPlayer;
    }
}
