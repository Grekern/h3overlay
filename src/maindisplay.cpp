#include "maindisplay.h"
#include "ui_maindisplay.h"

#include <QMouseEvent>
#include <QDebug>


MainDisplay::MainDisplay(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainDisplay),
    holdingLeft(false)
{
    ui->setupUi(this);
    QWidget::setWindowFlags( Qt::Window|
                             Qt::FramelessWindowHint|
                             Qt::WindowSystemMenuHint|
                             Qt::CustomizeWindowHint);

    this->ui->redFlag->setMaxFontSize(25);
    this->ui->blueFlag->setMaxFontSize(25);

    connect(this, &MainDisplay::textUpdated, this->ui->title,          &ResizeLabel::fitLabelToContainer);
    connect(this, &MainDisplay::textUpdated, this->ui->redMoney,       &ResizeLabel::fitLabelToContainer);
    connect(this, &MainDisplay::textUpdated, this->ui->redPlayerName,  &ResizeLabel::fitLabelToContainer);
    connect(this, &MainDisplay::textUpdated, this->ui->redRating,      &ResizeLabel::fitLabelToContainer);
    connect(this, &MainDisplay::textUpdated, this->ui->redFlag,        &ResizeLabel::fitLabelToContainer);
    connect(this, &MainDisplay::textUpdated, this->ui->blueMoney,      &ResizeLabel::fitLabelToContainer);
    connect(this, &MainDisplay::textUpdated, this->ui->bluePlayerName, &ResizeLabel::fitLabelToContainer);
    connect(this, &MainDisplay::textUpdated, this->ui->blueRating,     &ResizeLabel::fitLabelToContainer);
    connect(this, &MainDisplay::textUpdated, this->ui->blueFlag,       &ResizeLabel::fitLabelToContainer);

}

void MainDisplay::setTitle(const QString &title)
{
    this->ui->title->setText(title);
}

MainDisplay::~MainDisplay()
{
    delete ui;
}

void MainDisplay::update( const std::array<displayPlayerInfoStruct, 2> &displayData)
{

    this->ui->redTown->setStyleSheet("QFrame{ border-image:  url(:/images/towns/" +
                                     displayData[Left].town + ".png) 0 0 0 0 stretch stretch;}");
    this->ui->blueTown->setStyleSheet("QFrame{border-image:  url(:/images/towns/" +
                                      displayData[Right].town + ".png) 0 0 0 0 stretch stretch;}");

    this->ui->redHero->setStyleSheet("QFrame{border-image:  url(:/images/heroes/Hero_" +
                                     displayData[Left].hero + ".png) 0 0 0 0 stretch stretch;}");
    this->ui->blueHero->setStyleSheet("QFrame{border-image:  url(:/images/heroes/Hero_" +
                                      displayData[Right].hero + ".png) 0 0 0 0 stretch stretch;}");

    this->ui->redMoney->setText(displayData[Left].money);
    this->ui->blueMoney->setText(displayData[Right].money);

    this->ui->redPlayerName->setText(displayData[Left].name);
    this->ui->bluePlayerName->setText(displayData[Right].name);
    this->ui->redRating->setText(displayData[Left].rating);
    this->ui->blueRating->setText(displayData[Right].rating);

    this->ui->redFlag->setText(displayData[Left].wins);
    this->ui->blueFlag->setText(displayData[Right].wins);

    QPalette palette;
    palette.setColor(QPalette::WindowText, QColor(displayData[Left].nameColor));
    this->ui->redPlayerName->setPalette(palette);
    palette.setColor(QPalette::WindowText, QColor(displayData[Right].nameColor));
    this->ui->bluePlayerName->setPalette(palette);
    emit textUpdated();
}

void MainDisplay::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton )
    {
        this->dragingOldWindowPos = event->globalPosition();
        this->holdingLeft = true;
    }
}

void MainDisplay::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton )
    {
        this->holdingLeft = false;
    }
}

void MainDisplay::mouseMoveEvent(QMouseEvent *event)
{
    if (this->holdingLeft)
    {
        const QPointF delta = event->globalPosition() - this->dragingOldWindowPos;
        move(x()+delta.x(), y()+delta.y());
        this->dragingOldWindowPos = event->globalPosition();
    }
}

