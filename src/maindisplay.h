#ifndef MAINDISPLAY_H
#define MAINDISPLAY_H

#include <QWidget>
#include <QList>

#include "gamestructs.h"

namespace Ui {
class MainDisplay;
}

class MainDisplay : public QWidget
{
    Q_OBJECT

public:
    explicit MainDisplay(QWidget *parent = nullptr);

    /**
     * @brief setTitle Sets the title, this is never set automatically so
     * it has its own function call.
     * @param title The text to be shown as the title.
     */
    void setTitle(const QString &title);
    ~MainDisplay();

public slots:
    /**
     * @brief update A slot to send updated information to be displayed
     * @param displayData The data that will be displayed
     */
    void update(const std::array<displayPlayerInfoStruct, 2> &displayData);

signals:
    void textUpdated();

private:
    Ui::MainDisplay *ui;

    /**
     * @brief mousePressEvent Registers when LMB has been pressed to be used
     * for dragging the window.
     * @param event The mouse event.
     */
    void mousePressEvent(QMouseEvent *event);

    /**
     * @brief mouseReleaseEvent Registers when LMB has been released to be used
     * for stop dragging the window.
     * @param event The mouse event.
     */
    void mouseReleaseEvent(QMouseEvent *event);

    /**
     * @brief mouseMoveEvent Moves the window if the LMB is hold while on top
     * of the window
     * @param event The mouse event
     */
    void mouseMoveEvent(QMouseEvent *event);


    /** * * * * * * * * * *
     *  Member variables  *
     ** * * * * * * * * * */

    QPointF dragingOldWindowPos;
    bool holdingLeft;
};

#endif // MAINDISPLAY_H
