## USAGE
This application requires elevated permissions to attach to Heroes 3 HotA.  
Simply start this application and it should automatically try to attach to Heroes 3 HotA.  
It will look for a process named "h3hota.exe" or "h3hota HD.exe".   
If the process is named differently, it will not work.  
As soon as a game is active, the overlay should be automatically populated.
You can override the values in the "controller".

## HOW IT WORKS
The application reads the game memory so if the game is updated,
the application might stop working.
Data sources:
* Starter Hero/Town: Taken from "match info"   
   (Low/no risk of breaking on HD update, might break on HotA update)   
* Trade: Taken from game lobby chat. H3Overlay must be running before match start for it to work.   
   (Low/no risk of braking on HD update, might break on HotA update)   
* Rating opponent: Taken from the lobby list ingame   
   (Medium risk of breaking on HD update)   
* Your rating: Taken from top left corner in lobby, I think..   
   (Low/no risk of braking on HD update, might break on HotA update)   
* Win/Loss: Scanning for the popup containing "YOU WIN!"/"YOU LOST!" .   
   (Should be stable on HD updates, might break on HotA update)   


## BUILDING
This was compiled using QtCreator 11.0.2, Qt 6.6.0, GCC 11.2.0, 64 bit.

Get Qt here: https://www.qt.io/download-open-source.  
(Direct link to download page): https://www.qt.io/download-qt-installer-oss  
The default installation should include MinGW.  
Open QtCreator and load the project file "h3overlay.pro"  
Compile.

To make a standalone package, the binary need some libraries in the same directory as the exe.  
This can be done with the "windeployqt.exe" which is in the \<Qt installation>/mingw_64/bin/  
```
  $ windeployqt.exe <path to the directory where the compiled exe is>
```
It must have the same build environment as when compiling.  

Can be added as a custom build step in QtCreator. In the "Projects" tab in QtCreator, 
add a "Custom Process Step" with the following:  
```
Command: %{ActiveProject:QT_HOST_BINS}\windeployqt.exe
Argument: %{buildDir}\release
Working directory: %{buildDir}
```

## CONTACT
email: gitgrek@gmail.com